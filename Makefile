IMG=amap.img
SCRS=able-forth/forth.scr \
	extra.scr \
	atest/atest.scr \
	astring/astring.scr \
	aslab/aslab.scr \
	amap/amap.scr \
	amap/maptype-s.scr \
	amap-tests.scr

BLKS+=${SCRS:M*.scr:R:S/$/.blk/}

.include "config.mk"

.SUFFIXES: .scr .blk
.scr.blk:
	${SCR} ${SCRFLAGS} ${.IMPSRC} ${.TARGET}

.PHONY: build clean

build: ${IMG}

clean:
	-rm -vf ${IMG}
	-rm -vf *.blk

${IMG}: able-forth/forth.img ${BLKS}
	@echo ${BLKS}
	cp able-forth/forth.img ${IMG}
	img -t ${IMG} 3M
	${BLKLD} ${BLKLDFLAGS} -o ${IMG} ${BLKS}
